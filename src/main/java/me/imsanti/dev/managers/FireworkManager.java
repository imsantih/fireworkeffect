package me.imsanti.dev.managers;

import me.imsanti.dev.objects.ConfigFirework;
import me.imsanti.dev.objects.CustomFirework;
import me.imsanti.dev.utils.FireworkUtils;
import org.bukkit.Bukkit;
import org.bukkit.FireworkEffect;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class FireworkManager {

    private final me.imsanti.dev.FireworkEffect fireworkEffect;

    public FireworkManager(final me.imsanti.dev.FireworkEffect fireworkEffect) {
        this.fireworkEffect = fireworkEffect;
    }
    public void spawn(final Player player, final ConfigFirework configFirework) {
        final FireworkEffect effect = FireworkEffect.builder().trail(true).with(FireworkEffect.Type.valueOf(configFirework.getEffectName().toUpperCase())).withColor((FireworkUtils.getColors(configFirework.getColors()))).build();
        new CustomFirework(effect, player.getLocation().add(configFirework.getX(), configFirework.getY(), configFirework.getZ()));
    }

    public void loadFireworks() {
        final FileConfiguration config = fireworkEffect.getConfigFile().getConfig();
        config.getConfigurationSection("fireworks").getKeys(false).forEach(path -> {
            final ConfigurationSection fireworkSection = config.getConfigurationSection("fireworks." + path);
            final ConfigFirework configFirework = new ConfigFirework(fireworkSection.getString("command").toLowerCase(), fireworkSection.getInt("power"), fireworkSection.getString("effect").toUpperCase(), fireworkSection.getStringList("colors"), fireworkSection.getInt("position.x"), fireworkSection.getInt("position.y"), fireworkSection.getInt("position.z"), fireworkSection.getString("permission"));
            fireworkEffect.getLoadedFireworks().put(configFirework.getCommand(), configFirework);
            Bukkit.getConsoleSender().sendMessage("Loaded " + path.toUpperCase() + " firework.");
        });
    }

}


