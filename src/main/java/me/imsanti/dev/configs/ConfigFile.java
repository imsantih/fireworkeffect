package me.imsanti.dev.configs;

import me.imsanti.dev.FireworkEffect;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class ConfigFile {

    private final FireworkEffect fireworkEffect;

    public ConfigFile(ConfigManager configManager, final FireworkEffect fireworkEffect) {
        this.configManager = configManager;
        this.fireworkEffect = fireworkEffect;
    }

    private final ConfigManager configManager;

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(fireworkEffect.getDataFolder()), "config")) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(fireworkEffect.getDataFolder()), "config");
    }

    public File getConfigFile() {
        return new File(fireworkEffect.getDataFolder(), "config.yml");
    }
}
