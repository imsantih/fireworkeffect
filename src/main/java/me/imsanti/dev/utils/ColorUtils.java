package me.imsanti.dev.utils;

import org.bukkit.ChatColor;

public class ColorUtils {

    public static String color(final String fromText) {
        return ChatColor.translateAlternateColorCodes('&', fromText);
    }
}
