package me.imsanti.dev;

import me.imsanti.dev.commands.TestCommand;
import me.imsanti.dev.configs.ConfigFile;
import me.imsanti.dev.configs.ConfigManager;
import me.imsanti.dev.listeners.PlayerListener;
import me.imsanti.dev.managers.FireworkManager;
import me.imsanti.dev.objects.ConfigFirework;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public final class FireworkEffect extends JavaPlugin {

    private final FireworkManager fireworkManager = new FireworkManager(this);
    private final ConfigManager configManager = new ConfigManager(this);
    private final ConfigFile configFile = new ConfigFile(configManager, this);
    private final Map<String, ConfigFirework> loadedFireworks = new HashMap<>();

    @Override
    public void onEnable() {
        createConfig();
        fireworkManager.loadFireworks();
        registerEvents();
        registerCommands();
        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public FireworkManager getFireworkManager() {
        return fireworkManager;
    }

    private void registerCommands() {
        getCommand("testcommand").setExecutor(new TestCommand(this));
    }

    private void createConfig() {
        if(configFile.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created config.yml");

        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded config.yml");


        }

    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
    }

    public Map<String, ConfigFirework> getLoadedFireworks() {
        return loadedFireworks;
    }

    public ConfigFile getConfigFile() {
        return configFile;
    }
}
