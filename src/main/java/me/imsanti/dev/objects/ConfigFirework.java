package me.imsanti.dev.objects;

import java.util.List;

public class ConfigFirework {

    private final String command;
    private final int power;
    private final String effectName;
    private final List<String> colors;
    private final int x;
    private final int y;
    private final int z;
    private final String permission;

    public ConfigFirework(final String command, final int power, final String effectName, final List<String> colors, final int x, final int y, final int z, final String permission) {
        this.command = command;
        this.power = power;
        this.effectName = effectName;
        this.colors = colors;
        this.x = x;
        this.y = y;
        this.z = z;
        this.permission = permission;
    }

    public int getPower() {
        return power;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public List<String> getColors() {
        return colors;
    }

    public String getCommand() {
        return command;
    }

    public String getEffectName() {
        return effectName;
    }

    public String getPermission() {
        return permission;
    }
}
