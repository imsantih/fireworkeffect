package me.imsanti.dev.commands;

import me.imsanti.dev.FireworkEffect;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class TestCommand implements CommandExecutor {

    private final FireworkEffect fireworkEffect;

    public TestCommand(final FireworkEffect fireworkEffect) {
        this.fireworkEffect = fireworkEffect;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        final Player player = (Player) sender;

        fireworkEffect.getFireworkManager().spawn(player, fireworkEffect.getLoadedFireworks().get("firework1"));
        return true;
    }
}
