package me.imsanti.dev.listeners;

import me.imsanti.dev.FireworkEffect;
import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class PlayerListener implements Listener {

    private final FireworkEffect fireworkEffect;

    public PlayerListener(final FireworkEffect fireworkEffect) {
        this.fireworkEffect = fireworkEffect;
    }

    @EventHandler
    private void HandleFirework(final PlayerCommandPreprocessEvent event) {
        if(fireworkEffect.getLoadedFireworks().containsKey(event.getMessage().toLowerCase().replaceAll("/", ""))) {
            if(event.getPlayer().hasPermission(fireworkEffect.getLoadedFireworks().get(event.getMessage().toLowerCase().replaceAll("/", "")).getPermission())) {
                event.setCancelled(true);
                fireworkEffect.getFireworkManager().spawn(event.getPlayer(), fireworkEffect.getLoadedFireworks().get(event.getMessage().toLowerCase().replaceAll("/", "")));
                return;
            }

            event.setCancelled(true);
            event.getPlayer().sendMessage(ColorUtils.color("&cNo permissions"));


        }
    }
}
